Problem: given a list of boxes... find the largest number of boxes that can fit into each other
         e.g b1 -> b2 -> b3 -> ... -> bm which says b1 fits into b2 which fits into b3 etc
Current status: Unsolved
progress: Can currently tell which box can fit into which boxes
What's left: form possible trees and pic the path of the tree with the biggest height