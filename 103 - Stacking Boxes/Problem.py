
def swap(box , i , j): # takes a box and indices i and j and swaps items 
    temp = box[i]
    box[i] = box[j]
    box[j] = temp

def stringToIntArr(box): # Takes a box with dimensions in type str and returns a box with dimensions in type int
    for i in range(len(box)):
        box[i] = int(box[i])

def boxInput(k , n):   # where k is the number of boxes and n is the number of dimensions
    boxes = []
    i = 0
    while (i < k):
        box = input().split(' ')
        stringToIntArr(box)
        if len(box) == n :
            boxes.append(box)
            i += 1
        else:
            print('Enter a box of valid dimensions')
         
    return boxes

def x_into_y(box_a , box_b): # See if box x fits into box y and return a boolean value
    #check if box a fits into box b and return true or false
    temp_b , temp_a = box_b.copy() , box_a.copy()
    temp_b.sort()
    temp_a.sort()
    # print('box a sorted is' , temp_a , '\nbox b sorted is', temp_b)
    canFit = True
    for i in range(len(temp_a)) :
        a = temp_a[i]
        b = temp_b[i]
        #print(a , b)
        if a > b:
            canFit = False
            break     

    return canFit   

def solveSeq(boxes):
    n = len(boxes)
    seq = []

    for i in range(n-1):
        for j in range(i+1,n):
            box_a = boxes[i]
            box_b = boxes[j]
            # see if a fits into b if not check if b fits into a
            if(x_into_y(box_a , box_b)):
                seq.append([i+1,j+1])
            elif(x_into_y(box_b , box_a)):
                seq.append([j+1,i+1])
    seq.sort()
    return(seq)



print(solveSeq(boxInput(8,6)))