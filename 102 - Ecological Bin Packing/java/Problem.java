import java.util.*;

class Problem {
    public static void main(String[] args) {
        
        HashMap< String, int[]> bins = new HashMap<>();
        HashMap <String , int[]> configs = new HashMap<>();
        Scanner input = new Scanner(System.in);
        
        while(1 > 0){
        String s = input.nextLine();
        int[] intArray = stringArrayToIntArray(s.split(" "));
        fillHash(bins, intArray);
        colorConfig(configs, bins);
        solve(configs);
        }  

    }

    public static int[] stringArrayToIntArray(String[] strArray){
        int[] temp = new int[9];
        for(int i = 0 ; i < 9 ; i++){
            temp[i] = Integer.parseInt(strArray[i]);
        }
        return temp;
    }

    public static void fillHash(HashMap<String , int[]> bins , int[] intArray){
        bins.put("bin1", Arrays.copyOfRange(intArray, 0, 3));
        bins.put("bin2", Arrays.copyOfRange(intArray, 3, 6));
        bins.put("bin3", Arrays.copyOfRange(intArray, 6, 9));
    }

    public static void colorConfig( HashMap<String, int[]> configs , HashMap<String , int[]> bins){
       int[][] arr = new int[3][3];

        for(int i = 0 ; i<3; i++){
            arr[i][0] = bins.get("bin1")[i] + bins.get("bin2")[i];
            arr[i][1] = bins.get("bin1")[i] + bins.get("bin3")[i];
            arr[i][2] = bins.get("bin2")[i] + bins.get("bin3")[i];
        }  
        configs.put("G",arr[0]);
        configs.put("B",arr[1]);
        configs.put("C",arr[2]);
    }

    public static void solve(HashMap<String , int[]> configs){
        String[] Possibles = {"BCG" , "BGC" , "CBG","CGB","GBC" , "GCB"  };
        int max = (int)Math.pow(2,31);
        ArrayList<String> ans = new ArrayList<>();
        String conf = "";
        HashMap<String, Integer> types = new HashMap<>();
        typesConf(types);
        
        for (String string : Possibles) {
            String[] s = string.split("");
            int temp = configs.get("B")[types.get(s[0])] + configs.get("G")[types.get(s[1])] + configs.get("C")[types.get(s[2])] ;
            if(temp < max){
                conf = string;
                max = temp;
            }
        }
        System.out.println( conf + " " + max  );
    }

    public static void typesConf(HashMap<String, Integer> types){
        types.put("G", 2);
        types.put("B", 1);
        types.put("C", 0);
    }
}