import numpy as np

i = int(input())
j = int(input())

List = np.array([])
for k in range(i , j+1):
    count = 1
    h = k
    while h != 1:
        if h%2 != 0:
            h = 3*h + 1
            count += 1
        else:
            h = h/2
            count += 1

    List = np.append(List , [count] )

List = np.sort(List)

print("%d %d %d" %(i , j , List[j-i]))
