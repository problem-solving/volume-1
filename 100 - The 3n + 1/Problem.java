import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

class Problem{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i = in.nextInt();
        int j = in.nextInt();
        in.close();
        calc(i, j);
    }

    public static void calc(int i , int j){
        ArrayList<Integer> list = new ArrayList<>();

        for(int k = i ; k <= j ; k++){
            int count = 1;
            int h = k;
            while( h != 1 & h > 0){
                if(h%2 != 0){
                    h = 3*h + 1;
                    count ++;
                }
                else{    
                    h = h/2;
                    count++;
                }
            }
            list.add(count);
        }
        Collections.sort(list);
        System.out.printf("%d %d %d\n",i , j , list.get(j-i));
    }
}