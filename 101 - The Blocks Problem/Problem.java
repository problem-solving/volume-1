import java.util.*;



class Problem{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numStacks = in.nextInt();

        if(numStacks < 0 || numStacks > 24)
            return;

        ArrayList< ArrayList<Integer> > list = initList(numStacks);
        int[] positions = initArray(numStacks);


        move_a_onto_b(list, positions, 9, 1);
        move_a_over_b(list, positions, 8, 1);
        move_a_over_b(list, positions, 7, 1);
        move_a_over_b(list, positions, 6, 1);
        pile_a_over_b(list, positions, 8, 6);
        pile_a_over_b(list, positions, 8, 5);
        move_a_over_b(list, positions, 2, 1);
        move_a_over_b(list, positions, 4, 9);

  
        System.out.println();
        //move_a_onto_b(list, positions, 4, 8);
    
        // String command = in.nextLine();
        // while(!command.toLowerCase().equals("quit")){

        // }
       printList(list);
        
    }

    private static int[] initArray(int numStacks) {
        int[] temp = new int[numStacks];
        for(int i = 0 ; i < numStacks ; i++){
            temp[i] = i;
        }
        return temp;
    }

    public static ArrayList<ArrayList<Integer>> initList(int numStacks) {
        
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        for(int i = 0; i < numStacks ; i++){
            ArrayList<Integer> temp = new ArrayList<>();
            temp.add(i);
            list.add(temp);
        }
        return list;
    }

    public static void printList(ArrayList<ArrayList<Integer>> list){

        for(int i = 0; i < list.size() ; i++){
            System.err.printf("%d: ",i);
            for(int j = 0 ; j < list.get(i).size(); j++){
                    System.out.printf("%d " , list.get(i).get(j));        
            }
            System.out.println();
        }

    }

    public static void move_a_onto_b(ArrayList<ArrayList<Integer>> list, int[] positions, int a, int b){

        if(positions[a] == positions[b]) return; 

        int aIndex = positions[a];
        int aSize = list.get(aIndex).size();

        move_e(list, positions, a);
        list.get(aIndex).remove(list.get(aIndex).get(aSize-1));
        
        int bIndex = positions[b];
        move_e(list, positions, b);
        
        // now stack a on b
        list.get(bIndex).add(a);
        positions[a] = positions[b];
       
    }


    public static void pile_a_onto_b(ArrayList<ArrayList<Integer>> list, int[] positions, int a, int b){

        if(positions[a] == positions[b]) return;

        int aIndex = positions[a];
        int bIndex = positions[b];
        

        move_e(list, positions, b);
        ArrayList<Integer> temp = list.get(aIndex);
        boolean found = false;
        for(int i = 0 ; i < temp.size(); i++){
            if(temp.get(i) == a) found = true;
            if(found){
                list.get(bIndex).add(temp.get(i));
                positions[temp.get(i)] = positions[b];
                
            }
        }

         found = false;
        for(int i = 0 ; i < temp.size(); i++){
            if(temp.get(i) == a) found = true;
            if(found && list.get(aIndex).isEmpty() == false){
               list.get(aIndex).remove(temp.get(i));
               i--;
            }
        }

      
    }

    public static void move_a_over_b(ArrayList<ArrayList<Integer>> list, int[] positions, int a, int b){
        if(positions[a] == positions[b]) return;

        int aIndex = positions[a];
        int aSize = list.get(aIndex).size();

        move_e(list, positions, a);
        list.get(aIndex).remove(list.get(aIndex).get(aSize-1));
        
        int bIndex = positions[b];
        // now stack a over b
        list.get(bIndex).add(a);
        positions[a] = positions[b];
        //printList(list);
    }

    public static void pile_a_over_b(ArrayList<ArrayList<Integer>> list, int[] positions, int a, int b){
        if(positions[a] == positions[b]) return;

        int aIndex = positions[a];
        int bIndex = positions[b];
        

       // move_e(list, positions, b);
        ArrayList<Integer> temp = list.get(aIndex);
        boolean found = false;
        for(int i = 0 ; i < temp.size(); i++){
            if(temp.get(i) == a) found = true;
            if(found){
                list.get(bIndex).add(temp.get(i));
                positions[temp.get(i)] = positions[b];
                
            }
        }

         found = false;
        for(int i = 0 ; i < temp.size(); i++){
            if(temp.get(i) == a) found = true;
            if(found){
               list.get(aIndex).remove(temp.get(i));
               i--;
            }
        }

    }

    public static void move_e(ArrayList<ArrayList<Integer>> list, int[] positions, int a){
        int aIndex = positions[a];
        int aSize = list.get(aIndex).size();
        while(list.get(aIndex).get(aSize-1) != a){
            int num = list.get(aIndex).get(aSize-1);
            positions[num] = num;
            list.get(num).add(num);
            // removing the final element
            list.get(aIndex).remove(list.get(aIndex).get(aSize-1));
            aSize--;
        }
    }
    
}